export type IAppSettings = {
  mountingPoint: string
  webpackDevServerPort: number
  backendUrl: string
  reckonPricing: string
  localUrl: string
  pricingInterval: number
  debugPort: number
  backendPort: number
}