import { Module } from '@nestjs/common';
import { Logger } from '@nestjs/common'

import { ReckonProxyController } from '@reckon/backend/controllers/reckon-backend.controller';
import { ReckonClientService } from '@reckon/backend/services/reckon-client.service'
import { RootController } from '@reckon/backend/controllers/root.controller'


@Module({
  imports: [],
  controllers: [
    RootController,
    ReckonProxyController
  ],
  providers: [
    ReckonClientService,
    Logger
  ],
})
export class AppModule {}
