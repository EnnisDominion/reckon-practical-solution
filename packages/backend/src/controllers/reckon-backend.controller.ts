import {Controller, Dependencies, Get, Inject} from '@nestjs/common';
import { ReckonClientService } from '@reckon/backend/services/reckon-client.service'
import { IPriceList } from '@reckon/core/types/IStockPricing'

@Controller('pricing')
export class ReckonProxyController {

  constructor(private reckonClient: ReckonClientService) {}

  @Get()
  async getPricing(): Promise<IPriceList> {
    return await this.reckonClient.getPricing()
  }
}
