import { Controller, Get, Render } from '@nestjs/common'
import settings from '@reckon/tools/settings.json'
import { IReckonClientConfig } from '@reckon/core/types/IReckonClientConfig'

interface IMountConfig {
  frontEndBundle: string,
  appConfig: string
}

@Controller()
export class RootController {

  @Get('/index.html')
  @Render('index')
  root(): IMountConfig{
    const appConfig: IReckonClientConfig = {
      urls: {
        pricingList: '/pricing'
      },
      pollInterval: settings.pricingInterval,
      backendUrl: settings.backendUrl,
      backendPort: settings.backendPort
    }

    return {
      // I would normally do something smarter like serve from a non local cdn
      frontEndBundle: `${settings.localUrl}:${settings.webpackDevServerPort}/bundle.js`,
      appConfig: JSON.stringify(appConfig)
    }

  }

}