import axios from 'axios'
import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common'

import settings from '@reckon/tools/settings.json'
import { IPriceList } from '@reckon/core/types/IStockPricing'
import { IAppSettings } from '@reckon/toolsISettings'


const config = settings as IAppSettings

@Injectable()
export class ReckonClientService {

  constructor(private loggerService: Logger) {}

  async getPricing(): Promise<IPriceList> {
    try {
      const response = await axios.get<IPriceList>(config.reckonPricing)
      return response.data
    } catch (e) {
      this.loggerService.log(`Failed to retrieve pricelist from ${config.reckonPricing}`, e)
    }
      return []
  }
}
