
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import handlebars  from 'handlebars'
import { join } from 'path'
import settings from '@reckon/tools/settings.json'
import { AppModule } from './app.module'


async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
      AppModule,
      new FastifyAdapter()
  );
  app.setViewEngine({
    engine: {
      handlebars: handlebars,
    },
    templates: join(__dirname, 'views')
  })
  app.useStaticAssets({
    root : join(__dirname, '..', 'public'),
    prefix: '/public/'
  })
  await app.listen(settings.backendPort);
}
bootstrap();
