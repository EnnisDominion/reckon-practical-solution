const settings = require('../tools/settings.json')
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin')
const nodeExternals = require('webpack-node-externals')
const StartServerPlugin = require('start-server-nestjs-webpack-plugin')
const CopyPlugin = require("copy-webpack-plugin");
const webpack = require('webpack')

const debugEnabledOnPort = settings.debugPort

module.exports = {
  entry: {
    Server: './src/main.ts'
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.tsx?|\.jsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {from: './src/views', to:'./views'}
      ]
    }),
    new webpack.HotModuleReplacementPlugin(),
    new StartServerPlugin({
      name: 'app.js',
      nodeArgs: ['--inspect'],
      signal: false | true | 'SIGUSR2',
      keyboard: true
    })
  ],
  watchOptions: {
    ignored: /node_modules/
  },
  externals: [nodeExternals({
    allowlist: [
      'webpack/hot/poll?1000'
    ]
  })],

  resolve: {
    alias: {
      handlebars: 'handlebars/dist/handlebars.js'
    },
    extensions: ['.ts', '.js'],
    plugins: [
      new TsConfigPathsPlugin({
        extensions: [
          '.ts',
          '.js'
        ]
      })
    ]
  },
  devtool: 'source-map',
  output: {
    filename: 'app.js',
  },
};