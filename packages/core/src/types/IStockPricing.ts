export interface IStockItem {
  code: string,
  price: number
}

export type IPriceList = Array<IStockItem>