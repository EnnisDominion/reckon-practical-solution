interface IUrls {
    pricingList: string
}

export interface IReckonClientConfig {
    urls: IUrls
    backendUrl: string
    backendPort: number
    pollInterval: number
}