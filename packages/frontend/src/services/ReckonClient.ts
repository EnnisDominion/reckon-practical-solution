import axios from 'axios'
import debug from 'debug'
import { IPriceList } from '@reckon/core/types/IStockPricing'
import { IReckonClientConfig } from '@reckon/core/types/IReckonClientConfig'

const logger = debug('frontend/ReckonClient')
export type IPriceRequest = {
  priceList: IPriceList
  date: Date
}

class ReckonClient implements IReckonClientConfig {

    urls: IReckonClientConfig['urls']
    backendPort
    backendUrl
    pollInterval

    constructor(config: IReckonClientConfig) {
      this.urls = config.urls
      this.backendPort = config.backendPort
      this.backendUrl = config.backendUrl
      this.pollInterval = config.pollInterval
    }

    async priceList(): Promise<IPriceRequest> {
        try {
          const response = await axios.get<IPriceList>(this.urls.pricingList)
          return {
            priceList: response.data,
            date: new Date()
          }
        } catch (e) {
          logger('Failed to retrieve the price list from ', this.urls.pricingList, e)
        }

        return {
          priceList: [],
          date: new Date()
        }
    }

    startPoll(cb:(priceList:IPriceRequest)=>void, method = this.priceList): ReturnType<typeof setInterval> {
      return setInterval(async()=>{
        const priceList = await method.bind(this)()
        cb(priceList)
      }, this.pollInterval)
    }

    stopPoll(interval: ReturnType<typeof setInterval>) {
      clearInterval(interval)
    }
}

export default ReckonClient