import React from 'react'
import { ThemeProvider } from 'react-jss'
import AppContainer from '@reckon/frontend/containers/AppContaner'
import { IAppContanerTheme } from '@reckon/frontend/containers/AppContaner.theme'
import { IReckonClientConfig } from '@reckon/core/types/IReckonClientConfig'

export type IAppProps = {
  config: IReckonClientConfig
  theme?: IAppContanerTheme
}

const App:React.FunctionComponent<IAppProps> = ({
  config,
  theme
})=> {
  return (
    <React.Fragment>
      <ThemeProvider theme={theme || {}}>
        <AppContainer config={config} />
      </ThemeProvider>
    </React.Fragment>
  )
}

export default App