import react from 'react'
import reactDom from 'react-dom'
import settings from '@reckon/tools/settings.json'
import App from '@reckon/frontend/app'
import { IReckonClientConfig } from '@reckon/core/types/IReckonClientConfig'

window.onload = () => {
  const mountingPoint = document.querySelector<HTMLDivElement>(settings.mountingPoint)

  if(mountingPoint){
    const config: IReckonClientConfig = JSON.parse(mountingPoint.dataset.componentProps as string)
    const component = react.createElement(App, {config})
    reactDom.render(component, mountingPoint)
  }
}

