import ReckonClient, {IPriceRequest} from '@reckon/frontend/services/ReckonClient'
import { useEffect, useState } from 'react'
import { IReckonClientConfig } from '@reckon/core/types/IReckonClientConfig'

const usePreferenceClientHook = (config: IReckonClientConfig) => {
  const [ client, setClient ] = useState<ReckonClient>()
  const [ mostRecentPrices, setMostRecentPrices ] = useState<IPriceRequest>()
  const [ priceLists, setPriceLists ] = useState<IPriceRequest[]>([])

  useEffect(()=>{
    setClient(new ReckonClient(config))
  }, [config])

  useEffect(() => {
    const poller = client?.startPoll((priceList)=>{
      setMostRecentPrices(priceList)
    })

    // Provide a cleanup step to prevent memory leaks
    return () => {
      if(poller){
        client?.stopPoll(poller)
      }
    }
  }, [client])

  useEffect(()=>{

    // Only update the price list if a list was returned
    if(mostRecentPrices && mostRecentPrices.priceList.length > 0){
      setPriceLists([
        ...priceLists,
        mostRecentPrices,
      ])
    }
  },[mostRecentPrices])

  return { priceLists }
}

export default usePreferenceClientHook