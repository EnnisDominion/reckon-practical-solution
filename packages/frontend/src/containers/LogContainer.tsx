import React from 'react'
import LogComponent from '@reckon/frontend/components/LogComponent'
import { IPriceRequest } from '@reckon/frontend/services/ReckonClient'

export type ILogContainerProps = {
  isPaused: boolean,
  pausedIndex?: number,
  priceList: IPriceRequest[],
  pauseLog: () => void
}

const LogContainer:React.FunctionComponent<ILogContainerProps> = ({
  isPaused,
  pausedIndex,
  priceList,
  pauseLog,
}) => {
  const loggedPriceList = isPaused
    ? priceList.slice(0,pausedIndex)
    : priceList

  const pauseLabel = isPaused
    ? 'Resume'
    : 'Pause Log'

  return (
    <React.Fragment>
      <LogComponent priceList={loggedPriceList} pauseLog={pauseLog} pauseLabel={pauseLabel} />
    </React.Fragment>
  )
}

export default LogContainer