import React, {useState} from 'react'
import merge from 'lodash/merge'

import usePreferenceClientHook from '@reckon/frontend/hooks/reckonClient.hook'
import { IReckonClientConfig }  from '@reckon/core/types/IReckonClientConfig'
import LogContainer from '@reckon/frontend/containers/LogContainer'
import SummaryContainer from '@reckon/frontend/containers/SummaryContainer'
import { createUseStyles, useTheme, ThemeProvider } from 'react-jss'
import AppContainerTheme, {IAppContanerTheme} from '@reckon/frontend/containers/AppContaner.theme'
import classNames from 'classnames'

const useStyles = createUseStyles((theme:Record<any, any>) => {
 return merge(
   theme,
   AppContainerTheme
 )
})

export type IAppProps =  {
    config: IReckonClientConfig
}

const AppContainer:React.FunctionComponent<IAppProps> = ({
  config,
})=> {
  const { priceLists } = usePreferenceClientHook(config)
  const [ pausedIndex, setPausedIndex ] = useState<number|undefined>()
  const theme = useTheme()
  const classes = useStyles(theme|| {})

  function pauseLog(){
    if(!!pausedIndex){
      setPausedIndex(undefined)
    } else {
      setPausedIndex(priceLists.length -1)
    }
  }

  const isPaused = !!pausedIndex

  return (
    <React.Fragment>
      <div className={classNames(
          classes.block
      )}>
        <LogContainer isPaused={isPaused} pausedIndex={pausedIndex} priceList={priceLists} pauseLog={pauseLog} />
        <SummaryContainer priceLists={priceLists} />
      </div>
    </React.Fragment>
  )
}

export default AppContainer