export type IAppContanerTheme = {
  block: any
  SummaryComponent?: any
  LogComponent?: any,
}

const AppContainerTheme: IAppContanerTheme = {
  block: {
    height: '100vh',
    display: 'grid',
    gridTemplateColumns: '1fr 2fr',
    gridColumnGap: '25px'
  }
}

export default AppContainerTheme