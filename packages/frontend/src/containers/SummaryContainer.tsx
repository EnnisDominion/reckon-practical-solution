import React from 'react'
import SummaryComponent, {ISummaryComponentProps, ISummaryRow} from '@reckon/frontend/components/SummaryComponent'
import { IPriceRequest } from '@reckon/frontend/services/ReckonClient'
import { IStockItem } from '@reckon/core/types/IStockPricing'

export type ISummaryContainerProps = {
  priceLists: IPriceRequest[]
}

const SummaryContainer: React.FunctionComponent<ISummaryContainerProps> = ({
  priceLists
}) => {

  /**
   * Create and initial row record for the summary
   * @param sockItem
   */
  const summaryRowFactory = ({
    price,
  }:IStockItem): ISummaryRow => {
    return {
      current: price,
      highest: price,
      lowest: price,
      starting: price
    }
  }

  /**
   * Aggregates the stockItem against the SummaryRowItem
   * @param stockItem
   * @param summaryRowItem
   */
  const aggregateSummary = (
    {
      code,
      price
    }: IStockItem,
    {
      highest,
      lowest,
      starting
    }:ISummaryRow
  ): ISummaryRow => {

    return {
      highest: price > highest ? price: highest,
      lowest: price < lowest ? price: lowest,
      current: price,
      starting
    }
  }

  /**
   * Loop through the priceLists and create the Summary record
   */
  const summary= priceLists.reduce(
    (summary:ISummaryComponentProps['summary'],priceRequest) => {
      // Aggregate each priceListItem against the summary record
      priceRequest.priceList.forEach((priceListItem)=>{
        const {
          code
        } = priceListItem

        if(summary[code]){
          summary = {
            ...summary,
            [code]: aggregateSummary(priceListItem, summary[code])
          }
        }

        else {
          summary = {
            ...summary,
            [code]:  summaryRowFactory(priceListItem)
          }
        }
      }
    )

    return summary
  }, {})

  return <SummaryComponent summary={summary} />
}

export default SummaryContainer