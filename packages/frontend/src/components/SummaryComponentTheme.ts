export type ISummaryComponentTheme = {
  block: any,
  summaryTable: any,
}

const SummaryComponentTheme: ISummaryComponentTheme = {
  block: {},
  summaryTable: {
    width: '100%',
    border: '5px',
    borderStyle: 'double',
    borderColor: 'black',
    // This keeps it the same as the margin ol applies
    paddingTop: '16px'
  }
}

export default SummaryComponentTheme