import React from 'react'
import format from 'date-fns/format'
import { ILogContainerProps } from '@reckon/frontend/containers/LogContainer'
import {createUseStyles, useTheme} from 'react-jss'
import merge from 'lodash/merge'
import LogComponentTheme, {ILogComponentTheme} from '@reckon/frontend/components/LogComponent.theme'
import classNames from 'classnames'

type ILogComponentProps = Pick<ILogContainerProps, 'priceList' | 'pauseLog'> & {
  pauseLabel: string
}

const useStyles = createUseStyles((theme:Record<any, any>): ILogComponentTheme => {
  return merge(
    theme.logComponent,
    LogComponentTheme
  )
})

const LogComponent:React.FunctionComponent<ILogComponentProps> = ({
  priceList,
  pauseLog,
  pauseLabel,
}) => {

  const theme = useTheme()
  const classes = useStyles(theme||{})

  return (
    <div className={classNames(classes.block)}>
      <div className={classNames(classes.logHeader)}>
        <h2>LOG</h2>
        <button className={classNames(
          classes.logButton
        )} onClick={pauseLog}>{pauseLabel}</button>
      </div>
      <div className={
        classNames(classes.logContainer)
      }>
        {
          priceList.map(priceListItem =>(
            <div>
              <ul className={classNames(classes.logListItem)}>
                <li>Updates for {format(priceListItem.date, 'yyyy-MM-dd hh:mm:ss')}</li>
                {priceListItem.priceList.map(item => (<li>{item.code}:{item.price}</li>))}
              </ul>
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default LogComponent