export type ILogComponentTheme = {
  block: any,
  logContainer: any
  logHeader: any
  logButton: any
  logListItem: any,
}

const LogComponentTheme: ILogComponentTheme = {
  block: {
    height: '100vh',
  },
  logHeader: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gridAutoFlow: 'column',
  },
  logButton:{
    alignSelf: 'center',
    width: 'fit-content',
    justifySelf: 'end',
    borderRadius: '15px',
  },
  logListItem: {
    listStyle: 'none',
    padding: '0px 15px'
  },
  logContainer: {
    overflowY:'scroll',
    height: '90vh',
    border: '5px',
    borderStyle: 'double',
    borderColor: 'black',
  }
}

export default LogComponentTheme