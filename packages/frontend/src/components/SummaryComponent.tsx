import React from 'react'
import merge from 'lodash/merge'
import {createUseStyles, useTheme} from 'react-jss'
import SummaryComponentTheme, {ISummaryComponentTheme} from '@reckon/frontend/components/SummaryComponentTheme'
import classNames from 'classnames'


export type ISummaryRow = {
  starting: number,
  lowest: number,
  highest: number,
  current: number
}

export type ISummaryComponentProps = {
  summary: {
    [key:string]: ISummaryRow
  }
}

type ISummaryRowProps = ISummaryRow & {
  stock: string
}

const useStyles = createUseStyles((theme:Record<any, any>): ISummaryComponentTheme => {
  return merge(
      theme.SummaryComponent,
      SummaryComponentTheme
  )
})

const SummaryRow: React.FunctionComponent<ISummaryRowProps> = ({
  starting,
  highest,
  lowest,
  current,
  stock,
}) => {
  return (
    <tr>
      <td>{stock}</td>
      <td>{starting}</td>
      <td>{lowest}</td>
      <td>{highest}</td>
      <td>{current}</td>
    </tr>
  )
}

const SummaryComponent: React.FunctionComponent<ISummaryComponentProps> = ({
  summary
}) => {
  const theme = useTheme()
  const classes = useStyles(theme || {})

  return (
    <div className={classNames(classes.block)}>
      <h2>Summary</h2>
      <div>
        <table className={classNames(classes.summaryTable)}>
          <thead>
            <tr>
              <td>Stock</td>
              <td>Starting</td>
              <td>Lowest</td>
              <td>Highest</td>
              <td>Current</td>
            </tr>
          </thead>
          <tbody>
            {
               Object.keys(summary).map(summaryItem => {
                 return (<SummaryRow {...summary[summaryItem]} stock={summaryItem}/>)
               })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default SummaryComponent