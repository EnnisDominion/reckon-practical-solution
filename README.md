# README #

This is effectively a skeleton for a mono repo using NestJS, React and Typescript

### How to setup
This project utilises yarn workspaces
1. To get started you'll need to have yarn installed on your machine `npm install --global yarn`
2. Run `yarn install` to install the dependencies
3. To start the backend run `yarn workspace @reckon-practical-test/backend build:dev`
4. To start the fronted run `yarn workspace @reckon-practical-test/frontend dev`
5. Then open the application on localhost:9898/index.html

### Workspaces
There are four workspaces included in this project
1. core
2. frontend
3. backend
4. tools